## Object oriented metrics

1. Clone or download  the project.
2. Import the project in Java IDE (preferably Eclipse).
3. Copy the java project you want to analyze to the "Project" directory.
4. Specify the project path in "MyMain.java".
5. Run the "MyMain.java".
6. After analysis, you will view the result in "output.csv".
