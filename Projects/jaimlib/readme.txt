Readme.txt for Jaimlib
----------------------

For an example of how to use Jaimlib see src/com/wilko/jaimtest/JaimTest.java

You can run the example using:

java -jar lib/jaimtest.jar <screenname> <password>

Use ctrl-c to end the test

For more information see http://jaimlib.sourceforge.net

wilko@users.sourceforge.net
