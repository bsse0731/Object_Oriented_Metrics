import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.comments.Comment;
import com.github.javaparser.ast.expr.BinaryExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

public class JavaClass implements Runnable{
	String filePath;
	String name;
	String parent;
	int numberOfChildren;
	int LCOM;
	int blankLine;
	int level;
	int LOC;
	int comment;
	long WMC;
	double commentPercentage;
	List<Comment> comments;
	ArrayList<Method> methodList;
	ArrayList<String> attributeList;

	public JavaClass(String filePath, String name) {
		this.filePath= filePath;
		this.name = name;
		this.parent = "";
		this.methodList = new ArrayList<Method>();
		this.attributeList = new ArrayList<String>();
		this.LCOM = 0;
		this.LOC = 0;
		this.level = 0;
		this.blankLine = 0;
		this.comment = 0;
		this.numberOfChildren = 0;
	}

	public void run() {
//		System.out.println(filePath + " running");
		boolean openingBracket = false;
		try{
			BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream(filePath)));

			String str;
			String temp;
			while((str=br.readLine())!=null){
				LOC++;
				/******************************** find blank line ********************************/
				if(str.length()==0){
					blankLine++;
				}

				if(openingBracket==false){
					/******************************** find parent ********************************/
					if(str.matches(".*(extends).*")){ 
						temp = str.substring(str.indexOf("extends")+("extends").length()).trim();
						if(temp.indexOf('{')!=-1){
							temp = temp.substring(0,temp.length()-1);
							temp = temp.trim();
						}
						parent = temp;
//						System.out.println(parent);
					}
					if(str.indexOf('{')!=-1){
						openingBracket = true;
					}
				}

			}
			br.close();
			this.extractCommentsandAttributesAndMethods();
			this.findAttributesUsedInMethods();
			this.countComment();
			
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	private void countComment() {
		for( Comment c: this.comments) {
			String lines[] = c.toString().split("\n");
			this.comment += lines.length;
		}
	}
	private void extractCommentsandAttributesAndMethods() {
		try{
			FileInputStream in = new FileInputStream(filePath);
			CompilationUnit cu = JavaParser.parse(in);
			comments = cu.getComments();
			
			MethodVisitor mv = new MethodVisitor();
			mv.javaClass = this;
			cu.accept(mv, null);
		} catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
	
	private void findAttributesUsedInMethods() {
		for (Method method: this.methodList) {
			for (String attribute :this.attributeList) {
				int index = method.body.indexOf(attribute);
				if(index !=-1){
//					System.out.println(method.body.charAt(index-1)+ " "+method.body.charAt(index+attribute.length()));
					if((Character.isLetterOrDigit(method.body.charAt(index-1))==false) && (Character.isLetterOrDigit(method.body.charAt(index+attribute.length())) == false)){
						method.attributesUsed.add(attribute);
					}
				}
			}
		}
	}

	public static class MethodVisitor extends VoidVisitorAdapter<Void> {
		JavaClass javaClass;
		@Override
		public void visit(MethodDeclaration md, Void arg) {
			Method m = new Method();
			m.name = md.getName().toString();
			m.body = md.getBody().toString();
//			System.out.println(md.);
			javaClass.methodList.add(m);
			super.visit(md, arg);
		}
		
		@Override
		public void visit(FieldDeclaration fd, Void arg) {
			for (VariableDeclarator attribute :fd.getVariables()) {
				javaClass.attributeList.add(attribute.toString());
			}
			super.visit(fd, arg);
		}
	}
}
