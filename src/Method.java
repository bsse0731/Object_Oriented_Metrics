
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

public class Method {
	String name;
	String body;
	Set<String> attributesUsed;
	Set<String> methodCalled;
	int CBO;
	int cyclomaticComplexity;
	int executableStatement;
	
	public Method() {
		this.methodCalled = new HashSet<String>();
		this.attributesUsed = new HashSet<String>();
		this.CBO = 0;
		this.executableStatement = 0;
	}
	
	/**************methods and instance variables defined by the other class***********/
	/***********************Executable statement and cyclomatic complexity***************************/
	
	public void cyclomaticComplexity() {
		int complexity = 1;
		
		String[] keywords = {"if","else",
							"while","for","do",
							"switch","case","default",
							"continue","break",
							"&&","||","?",":",
							"catch","finally","throw","throws"};

		StringTokenizer tokenizedString = new StringTokenizer(body);
		
		int returnCount = 0;
		
		while (tokenizedString.hasMoreTokens())
		{
			String words = tokenizedString.nextToken();
			for(String keyword: keywords)
			{
				if (keyword.equals("\\"))
                    break;
				
				if (keyword.equals(words))
				{
					complexity++;
				}
				else if(keyword.equals("return"))
				{
					returnCount++;
				}
			}
		}
		
		if(returnCount >= 1)
			returnCount--;	// Having one return
				
		this.cyclomaticComplexity = complexity+returnCount;
	}
}
