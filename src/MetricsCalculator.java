import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class MetricsCalculator {
	String directory;
	ArrayList<Thread> threadList;
	ArrayList<JavaClass> classList;

	public MetricsCalculator(String directory) {
		this.directory = directory;
		this.threadList = new ArrayList<Thread>();
		this.classList = new ArrayList<JavaClass>();
	}

	public void init() {
		scanDirectory(new File(directory));
		for (Thread thread : this.threadList) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		for(JavaClass jc : this.classList){
			jc.level = this.calculateDIT(jc);
			jc.commentPercentage = this.calculateCommentPercentage(jc);
			jc.LCOM = this.calculateLCOM(jc);
			jc.WMC = this.calculateWMC(jc);
		}
		
		this.outputMetrics();
	}

	private void outputMetrics() {
		PrintWriter pw;
		try {
			pw = new PrintWriter(new File("output.csv"));
			StringBuilder sb = new StringBuilder();
			sb.append("Class name");
			sb.append(',');
			sb.append("LOC");
			sb.append(',');
			sb.append("Comment Percentage");
			sb.append(',');
			sb.append("Number Of Children");
			sb.append(',');
			sb.append("Depth of inheritance");
			sb.append(',');
			sb.append("LCOM");
			sb.append(',');
			sb.append("WMC");
			sb.append('\n');
			
			for(JavaClass jc : this.classList){
//				System.out.println(jc.name);
				sb.append(jc.name);
				sb.append(',');
				sb.append(jc.LOC);
				sb.append(',');
				sb.append(jc.commentPercentage);
				sb.append(',');
				sb.append(jc.numberOfChildren);
				sb.append(',');
				sb.append(jc.level);
				sb.append(',');
				sb.append(jc.LCOM);
				sb.append(',');
				sb.append(jc.WMC);
				sb.append('\n');
//				break;
			}
			pw.write(sb.toString());
			pw.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private double calculateCommentPercentage(JavaClass jc) {
		return (double)(jc.comment/(jc.LOC-jc.blankLine));
	}

	private long calculateWMC(JavaClass jc) {
		long sum = 0;
		for(int i=0; i<jc.methodList.size();i++) {
			jc.methodList.get(i).cyclomaticComplexity();
			sum += jc.methodList.get(i).cyclomaticComplexity;
		}
		
		return sum;
	}
	
	private int calculateLCOM(JavaClass jc) {
		int notCommon = 0;
		int common = 0;
		for(int i=0; i<jc.methodList.size();i++) {
			for(int j=i+1; j<jc.methodList.size();j++) {
				Method method1 = jc.methodList.get(i);
				Method method2 = jc.methodList.get(j);
				Set<String> intersection = new HashSet<String>(method1.attributesUsed);
				intersection.retainAll(method2.attributesUsed);
				if(intersection.isEmpty()==true) {
					notCommon++;
				}
				else {
					common++;
				}
			}
		}
		return (notCommon - common);
	}

	private int calculateDIT(JavaClass jc) {
		if(jc.parent.equals("")==false && jc.level==0) {
			//			System.out.println(jc.name);
			JavaClass parent = findParent(jc);
			if(parent != null) {
				jc.level = calculateDIT(parent) + 1;
			}

			else {
				jc.level = 0;
			}
		}


		return jc.level;
	}

	JavaClass findParent(JavaClass currentClass){
		for(JavaClass jc : this.classList){
			if (jc.name.equals(currentClass.parent)){
				jc.numberOfChildren++;
				return jc;
			}
		}
		return null;
	}


	public void scanDirectory(File folder){
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile() && listOfFiles[i].getName().contains(".java")) {
				String filename = listOfFiles[i].getName();
				//System.out.println("File " + folder+"/"+filename);
				JavaClass javaClass= new JavaClass(folder+"/"+filename, filename.substring(0,filename.indexOf(".java")));
				Thread thread = new Thread(javaClass);
				thread.start();
				this.threadList.add(thread);
				this.classList.add(javaClass);
			} 
			else if (listOfFiles[i].isDirectory()) {
				scanDirectory(new File(folder+"/"+listOfFiles[i].getName()));
				//System.out.println("Fold " + folder+"/"+listOfFiles[i].getName());
			}
		}
	}
}
